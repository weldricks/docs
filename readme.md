#Weldricks API Documentation

##Contents
1. [Categories](#markdown-header-categories)
2. [Products](#markdown-header-products)
3. [Basket](#markdown-header-basket)
4. [Checkout](#markdown-header-checkout)
5. [Orders](#markdown-header-orders)
6. [Webhooks](#markdown-header-webhooks)


The following examples are provided using the latest [Guzzle PHP library](http://docs.guzzlephp.org/en/latest/index.html):

```php
use GuzzleHttp\Client;

$client = new Client();
```

##Categories

A category feed is available to populate your database at [https://affiliates.weldricks.co.uk/categories]

The response includes a nested array of categories with each having properites:

- **id:** The unique id assigned to the category
- **parent_id:** The id of the parent category in the nested structure (null for top level)
- **menu:** The category name as it appears in the site navigation menu
- **title:** The full title of the category
- **links:** An array of links to related data
    - **uri** contains address for more detailed information
- **categories:** The nested array of child categories

###Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/categories');
```

###Example Response
```javascript
{
  data: [
    {
      id: 102,
      parent_id: null,
      menu: "Embarrassing",
      title: "Embarrassing",
      href: "/embarrassing",
      updated_at: "2016-02-26 13:27:39",
      links: [
        {
          rel: "self",
          uri: "http://affiliates.weldricks.app/categories/102"
        }
      ],
      categories: {
        data: [
          {
            id: 203,
            parent_id: 102,
            menu: "Acne",
            title: "Acne",
            href: "/embarrassing/acne",
            updated_at: "2016-02-26 13:27:39",
            links: [
              {
                rel: "self",
                uri: "http://affiliates.weldricks.app/categories/203"
              }
            ],
            categories: {
              data: [
                {
                  id: 501,
                  parent_id: 203,
                  menu: "Treatments",
                  title: "Treatments",
                  href: "/embarrassing/acne/treatments",
                  updated_at: "2016-02-26 13:27:41",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/501"
                    }
                  ]
                },
                {
                  id: 959,
                  parent_id: 203,
                  menu: "Gels, Creams & Lotions",
                  title: "Gels, Creams & Lotions",
                  href: "/embarrassing/acne/gels-creams-and-lotions",
                  updated_at: "2016-02-26 13:27:43",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/959"
                    }
                  ]
                },
                {
                  id: 960,
                  parent_id: 203,
                  menu: "Washes & Scrubs",
                  title: "Washes & Scrubs",
                  href: "/embarrassing/acne/washes-and-scrubs",
                  updated_at: "2016-02-26 13:27:43",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/960"
                    }
                  ]
                },
                {
                  id: 961,
                  parent_id: 203,
                  menu: "Cleansers & Toners",
                  title: "Cleansers & Toners",
                  href: "/embarrassing/acne/cleansers-and-toners",
                  updated_at: "2016-02-26 13:27:43",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/961"
                    }
                  ]
                }
              ]
            }
          },
          {
            id: 204,
            parent_id: 102,
            menu: "Bad Breath",
            title: "Bad Breath",
            href: "/embarrassing/bad-breath",
            updated_at: "2016-02-26 13:27:39",
            links: [
              {
                rel: "self",
                uri: "http://affiliates.weldricks.app/categories/204"
              }
            ],
            categories: {
              data: [
                {
                  id: 641,
                  parent_id: 204,
                  menu: "Mouthwashes",
                  title: "Mouthwashes",
                  href: "/embarrassing/bad-breath/mouthwashes",
                  updated_at: "2016-02-26 13:27:41",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/641"
                    }
                  ]
                },
                {
                  id: 962,
                  parent_id: 204,
                  menu: "Sprays & Chewing Gums",
                  title: "Sprays & Chewing Gums",
                  href: "/embarrassing/bad-breath/sprays-and-chewing-gums",
                  updated_at: "2016-02-26 13:27:43",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/962"
                    }
                  ]
                },
                {
                  id: 1149,
                  parent_id: 204,
                  menu: "Toothpastes",
                  title: "Toothpastes",
                  href: "/embarrassing/bad-breath/toothpastes",
                  updated_at: "2016-02-26 13:27:44",
                  links: [
                    {
                      rel: "self",
                      uri: "http://affiliates.weldricks.app/categories/1149"
                    }
                  ]
                }
              ]
            }
          }
        ]
      ...
```

[Back to Top](#markdown-header-weldricks-api-documentation)

##Products

A product feed is available to populate your database at [https://affiliates.weldricks.co.uk/products]

The response includes a nested array of categories with each having properites:

- **id:** The unique id assigned to the product
- **name:** The full name of the product
- **price:** Current price for the item
- **rrp:** Recommended Retail Price for product
- **image:** Link to image on S3 server (**_NB: should be saved onto your server and linked to accordingly_**)
- **type:** the type of product listed:
    - **N** = No Type
    - **GSL** = General Sales Line
    - **P** = Pharmacist
    - **POM** = Prescription Only
    - **VET** = Vet Prescription
- **availability:** The availability status of the product
    - **InStock** The product is currently in stock and available for sale
    - **OutOfStock** The product is out of stock and cannot be bought.
    - **InStoreOnly** Not currently used
    - **PreOrder** Not currently used
- **updated_at:** The date the product was last updated
- **links:** The nested array of child categories
    - **uri** contains address for single product information
- **Tabs:** The nested array for product information, includes;
    - **Directions** Unique directions used for the product
    - **Warnings** Warnings that apply to the product
    - **Ingredients** Active and other ingredients used in the product
- **categories:** The nested array of categories the product is currently listed in

###Example Request

```php
$response = $client->request('GET', 'https://affiliates.weldricks.co.uk/products');
```

###Example Response

```javascript
{
  data: [
    {
      id: 1931,
      name: "Elgydium Toothpaste Anti-plaque 75ml",
      price: "2.79",
      rrp: "3.50",
      image: null,
      type: "N",
      availability: "OutOfStock",
      updated_at: "2016-04-13 15:48:17",
      links: [
        {
          rel: "self",
          uri: "http://affiliates.weldricks.app/products/1931"
        }
      ],
      tabs: {
        data: [
          {
            id: 6917,
            name: "Directions",
            html: "<p>Learning how to brush your teeth properly is the first step to maintaining healthy teeth and gums. Plus, it helps minimise the risk of tooth decay and gum disease, the major causes of tooth loss.</p> <h3>Before You Begin</h3> <p>While there are several methods of brushing teeth with a manual toothbrush, always ask your dental professional for their recommendation and be sure to follow their instructions. To start, use fluoride toothpaste with a soft-bristle toothbrush, and don&rsquo;t forget to replace it every three months.</p> <h3>Two Minutes, Twice a Day</h3> <p>To brush your teeth correctly, spend at least two minutes using a recommended technique, which includes 30 seconds brushing each section of your mouth (upper right, upper left, lower right and lower left), both morning and night. Since most manual toothbrushes don&rsquo;t have built-in two-minute timers, you may want to have a clock handy so you can be sure you&rsquo;re brushing long enough.</p> <h3>Positioning the Toothbrush</h3> <p><strong>How you hold the toothbrush depends on which part of the tooth you&rsquo;re brushing.</strong></p> <p> <ul> <li>Step 1: Start with outer and inner surfaces, and brush at a 45-degree angle in short, half-tooth-wide strokes against the gum line. Make sure you reach your back teeth.</li> <li>Step 2: Move on to chewing surfaces. Hold the brush flat and brush back and forth along these surfaces.</li> <li>Step 3: Once you get to the inside surfaces of your front teeth, tilt the brush vertically and use gentle up-and-down strokes with the tip of brush.</li> <li>Step 4: Be sure to brush gently along the gum line.</li> <li>Step 5: Brush your tongue in a back-to-front sweeping motion to remove food particles and help remove odour-causing bacteria to freshen your breath.</li> <li>Step 6: Try gently brushing the roof of your mouth for an extra-fresh feeling.</li> </ul> </p> <p>Now that you&rsquo;ve learned proper brushing technique, a little discipline in practicing it every day will help make it feel like second nature. It&rsquo;s one of the easiest things you can do to maintain the health of your teeth and gums.</p>",
            updated_at: "-0001-11-30 00:00:00"
          },
          {
            id: 6918,
            name: "Warnings",
            html: "<p>Do not swallow.</p> <p>In case of intake of fluoride from other sources consult a dentist or doctor.&nbsp;</p> <p>Sensitive teeth may indicate an underlying problem which needs prompt care by a dentist. See your dentist as soon as possible for advice.</p> <div><br /></div>",
            updated_at: "-0001-11-30 00:00:00"
          },
          {
            id: 6919,
            name: "Ingredients",
            html: "<p>Aque, calcium carbonate, glycerin, hydrated silica, sodium lauryl sulfate, aroma (flavour), cvhondrus crispus (carrageenan), Benzyl alcohol, cellulose gum, chlorhexidine digluconate, limonene, methylparaben, propylparaben, sodium saccharin, CI 77891 (titanium dioxide).</p>",
            updated_at: "-0001-11-30 00:00:00"
          }
        ]
      },
      categories: {
        data: [
          {
            id: 1138,
            parent_id: 223,
            menu: "Toothpaste Protection",
            title: "Toothpaste Protection",
            href: "/essentials/dental-care/toothpaste-protection",
            updated_at: "2016-02-26 13:27:44",
            links: [
              {
                rel: "self",
                uri: "http://affiliates.weldricks.app/categories/1138"
              }
            ]
          },
          {
            id: 1319,
            parent_id: 384,
            menu: "Offers",
            title: "Offers",
            href: "/offers/february-offers/offers",
            updated_at: "2016-02-26 13:27:45",
            links: [
              {
                rel: "self",
                uri: "http://affiliates.weldricks.app/categories/1319"
              }
            ]
          }
        ]
      }
    }
    ...
```

[Back to Top](#markdown-header-weldricks-api-documentation)


##Basket


[Back to Top](#markdown-header-weldricks-api-documentation)



##Checkout


[Back to Top](#markdown-header-weldricks-api-documentation)

##Orders

Updates to orders are provided via webhooks (see below).


##Webhooks


[Back to Top](#markdown-header-weldricks-api-documentation)
